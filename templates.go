// Copyright 2012 Yoshifumi Yamaguchi, All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.
package main

import (
	"text/template"
)

// activateTemplateText is a template for create `activate` file
// for a new Go workspace.
const activateTemplateText = `#!/bin/sh
function deactivate() {
  if [ -n "$GOENVNAME" ]; then
    unset GOENVNAME
  fi

  if [ -n "$_OLD_GOROOT" ]; then
    export GOROOT="$_OLD_GOROOT"
    unset _OLD_GOROOT
  fi

  if [ -n "$_OLD_PS1" ]; then
    export PS1="$_OLD_PS1"
    unset _OLD_PS1
  fi

  if [ -n "$_OLD_PATH" ]; then
    export PATH="$_OLD_PATH"
    unset _OLD_PATH
  fi

  if [ -n "$_OLD_GOPATH" ]; then
    export GOPATH="$_OLD_GOPATH"
    export GOBIN="$_OLD_GOPATH"/bin
    unset _OLD_GOPATH
  fi

  if [ "$1" != "init" ]; then
    unset -f deactivate
  fi
}

deactivate init

export _OLD_PATH="$PATH"
export _OLD_GOPATH="$GOPATH"
export _OLD_GOROOT="$GOROOT"
export _OLD_PS1="$PS1"
export GOENVNAME="{{.ENVNAME}}"
export PS1="(go:$GOENVNAME) $_OLD_PS1"
export GOPATH={{.GOPATHDIR}}/$GOENVNAME
export GOBIN=$GOPATH/bin
export PATH={{if .GAE}}"$GAEGO":{{end}}"$GOBIN"{{if .GOROOT}}:"$GOROOT/bin"{{end}}:$PATH
{{if .GOROOT}}export GOROOT={{.GOROOT}}{{end}}
`

var activateTemplate = template.Must(template.New("activate").Parse(activateTemplateText))

// appYamlTemplateText is a template for create `app.yaml` file
// for a new GAE/Go workspace.
const appYamlTemplateText = `application: {{ .EnvName }}    # change here
version: 1
runtime: go
api_version: go1

handlers:
- url: /.*
  script: _go_app
`

var appYamlTemplate = template.Must(template.New("app.yaml").Parse(appYamlTemplateText))

// appGoTemplateText is a template for create `<appname>.go` file
// for a new GAE/Go workspace.
const appGoTemplateText = `package {{ .EnvName }}

import (
	"net/http"
)

func init() {
	http.HandleFunc("/", handler)
}

func handler(w http.ResponseWriter, r *http.Request) {
	// some code
}
`

var appGoTemplate = template.Must(template.New("app.yaml").Parse(appGoTemplateText))

const vscodeTemplateText = `{
    "go.gopath": "{{ .Gopath }}",
}`

var vscodeTemplate = template.Must(template.New("settins.json").Parse(vscodeTemplateText))
