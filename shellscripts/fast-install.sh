#!/bin/sh
#
# Environment variable GOENVTARGET is install target of `goenv`
# Default install target i /usr/local/bin.

if ! type git 2>&1 > /dev/null; then
  echo "[goenv] To get goenv, install git"
  exit 0
fi
git clone --branch develop --depth 1 https://bitbucket.org/ymotongpoo/goenv.git
cd goenv
go build -o goenv *.go
chmod +x goenv

if [ -z $GOENVTARGET ]; then
  target="/usr/local/bin"
elif [ -d "$GOENVTARGET" ]; then
  target="$GOENVTARGET"
else
  echo "no such a directory: $GOENVTARGET"
  exit 0
fi

cp goenv "$target"


